## @css[title-green](Android Studio)

## @css[title](VS)

## @css[title-red](Rails Framework)

### @css[description](Akane Yamashita / 2018.02.05(Tue))

---

## @css[title](Agenda)

### @css[description](Ruby on RailsとAndroid Studioの違いについてお話しします👩‍🏫)


#### - VS. Framework Architecture
#### - VS. Control Logic
#### - VS. Control View
#### - VS. Provide API
---

## @css[title](VS. Framework Architecture)

## @css[title-red](Rails)
### @css[description](-> MVC)
<div class="image-inline">
![MVC](https://railstutorial.jp/chapters/images/figures/mvc_detailed.png)
</div>

---

## @css[title-green](Android Studio)
### @css[description](-> MVVM)
### @css[description](or MVP)
![MVVM](https://cdn.journaldev.com/wp-content/uploads/2018/04/android-mvvm-pattern.png)

---

## @css[title](VS. Control Logic)


## @css[title-red](Rails)
### @css[description](-> Controller, Model)

<div class="image-inline">
![MVC](https://railstutorial.jp/chapters/images/figures/mvc_schematic.png)
</div>
---

## @css[title-green](Android Studio)
### @css[description](-> Model, Fragements, Presenter)
![FRAGMENT](https://akira-watson.com/wp-content/uploads/2018/05/fragment-fragment_02.jpg)
---


## @css[title](VS. Control View)


## @css[title-red](Rails)
### @css[description](-> json)
### @css[description](slim / haml, erb or JS Frameworks(angular / Vue))
![JSON](assets/img/json.PNG)

---

## @css[title-green](Android Studio)
### @css[description](-> xml, style files or drawable)

<div class="image-inline">
![XMLICON](https://cloud.netlifyusercontent.com/assets/344dbf88-fdf9-42bb-adb4-46f01eedd629/674a517a-5330-4524-a6b2-9b6d4fb1475f/tab-bar-cons-1-preview-opt.png)
</div>
---

### @css[title-red](Rails can use ...)


![FORM_FOR](https://i.stack.imgur.com/RgtoA.png)
---

### form_for method
```slim
= form_for(@user) do |f|
  = f.label :uid do
    = f.input :uid
  = f.label :password do
    = f.input :password
  = f.label :rember_me do
    = f.check_box :remember_me
  = f.label "Change password" do 
    = f.check_box :change_passwd
  = f.submit "Log in"
```
---
### @css[title-green](Android can use ...)

![EDIT_TXT](https://i.stack.imgur.com/ypsXX.png)
---

### Drawing by xml
```xml
      <com.google.android.material.textfield.TextInputLayout
              android:layout_width="match_parent"
              android:layout_height="wrap_content"
              android:textColorHint="@color/colorPrimaryText" >
        <com.google.android.material.textfield.TextInputEditText
		android:id="@+id/user_email" 
		android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:hint="@string/user_email"/>
      </com.google.android.material.textfield.TextInputLayout>
      <com.google.android.material.textfield.TextInputLayout
              android:layout_width="match_parent"
              android:layout_height="wrap_content">
        <com.google.android.material.textfield.TextInputEditText
                android:id="@+id/user_password"
		android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:hint="@string/user_password"/>
      </com.google.android.material.textfield.TextInputLayout>

    <Button
            android:id="@+id/signin_btn"
            android:text="@string/signin_btn_text"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorMedically"/>
```
---

## Copy & Paste 😇
---
## @css[title](VS. Provide API)

## @css[title-red](Rails)
### @css[description](-> Gemfile)
![Gemfile](assets/img/Gemfile.PNG)

---

## @css[title-green](Android Studio)
### @css[description](-> Gradle)

---

## Thank you for listening🤗
